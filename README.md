# Jay Coughlan's COMP 4932 Assignment 2; H.261 Compression

This is the project I worked on for BCIT. Please do the following to use the program.

1. Open an Image; The Open Dialogue Box Automatically filters to .bmp but you can find .jpeg as well
2. Save the image; This program only saves as .jayPeg (clever, I know). This is my compressed version
3. You may use the compression button to compress the image without saving it. This will augment the image in the viewport.

to open a .jayPeg
1. Select File>Open
2. Change the Open Dialogue Box to filter for .JayPeg
3. Select your .jayPeg
4. Profit