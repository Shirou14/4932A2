﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Collections;
using System.IO;

namespace H261Compression
{
    public static class JCompression
    {
        public enum DATATYPE { JPG, BMP, JAY }
        public static DATATYPE dtType;
        public static FileStream ostrm;
        public static StreamWriter writer;
        public static TextWriter oldOut;
        public static double[] preY, preCr, preCb;

        public static int[] luminanceTable = {
            16, 11, 10, 16, 24, 40, 51, 61,
            12, 12, 14, 19, 26, 58, 60, 55,
            14, 13, 16, 24, 40, 57, 69, 56,
            14, 17, 22, 29, 51, 87, 80, 62,
            18, 22, 37, 56, 68, 109, 103, 77,
            24, 35, 55, 64, 81, 104, 113, 92,
            49, 64, 78, 87, 103, 121, 120, 101,
            72, 92, 95, 98, 112, 100, 103, 99
        };

        public static int[] chrominanceTable =
        {
            17, 18, 24, 47, 99, 99, 99, 99,
            18, 21, 26, 66, 99, 99, 99, 99,
            24, 26, 56, 99, 99, 99, 99, 99,
            47, 66, 99, 99, 99, 99, 99, 99,
            99, 99, 99, 99, 99, 99, 99, 99,
            99, 99, 99, 99, 99, 99, 99, 99,
            99, 99, 99, 99, 99, 99, 99, 99,
            99, 99, 99, 99, 99, 99, 99, 99
        };

        static JCompression()
        {
            oldOut = Console.Out;
        }

        public static int getRValue(double y, double cb, double cr)
        {
            //we re-add the value 128 here by removing the -128 in cb and adding it to Y
            //this compensates for the subtraction of 128 when converting from RGB to YUV
            return (int)Math.Round(y + 128 + 1.402 * (cr));
        }

        public static int getGValue(double y, double cb, double cr)
        {
            //we re-add the value 128 here by removing the -128 in cb and adding it to Y
            //this compensates for the subtraction of 128 when converting from RGB to YUV
            return (int)Math.Round(y + 128 - 0.344136 * (cb) - 0.714136 * (cr));
        }

        public static int getBValue(double y, double cb, double cr)
        {
            //we re-add the value 128 here by removing the -128 in cb and adding it to Y
            //this compensates for the subtraction of 128 when converting from RGB to YUV
            return (int)Math.Round(y + 128 + 1.772 * (cb));
        }

        public static double[,] compressChrominance(double[,] chroma, int imgWidth, int imgHeight)
        {
            int xp = 0, yp = 0;
            double[,] compChroma = new double[(int)Math.Ceiling((double)chroma.GetLength(0) / 2), (int)Math.Ceiling((double)chroma.GetLength(1)/2)];
            for (int y = 0; y < imgHeight && yp < compChroma.GetLength(1); y += 2)
            {
                xp = 0;
                for (int x = 0; x < imgWidth && xp < compChroma.GetLength(0); x += 2)
                {
                    compChroma[xp,yp] = chroma[x,y];
                    xp++;
                }
                yp++;
            }

            return compChroma;
        }

        public static double[] process(int width, int height, double[,] chromaChannel, int[] qTable)
        {
            //we first have to see if the image width and height are divisible by 8, and we'll save it into a variable
            int wBuffer = width % 8;
            int hBuffer = height % 8;
            //now we check to see if these are non zero, if so we augment it by subtracting it from 8 so when we add it on to our array, the size is divisible by 8
            if (wBuffer != 0)
                wBuffer = 8 - wBuffer;
            if (hBuffer != 0)
                hBuffer = 8 - hBuffer;
            //by doing this, if the width or height is divisible by 8, we're not adding unnecessary slots.
            //if it's not, we are adding only slots necessary to make it divisible by 8.

            //this method is to prime data as an 8x8 array to pass into the DCT and such
            double[] processed = new double[(width + (8 - (width % 8))) * (height + (8 - (height % 8)))], zTmp = new double[64];
            double[,] tmp = new double[8, 8];
            //we calculate out how many blocks there are width and height wise
            //we also keep track of the compressed index for when we enter it in later
            int xBlocks = (width + (8 - (width % 8))) / 8, yBlocks = (height + (8 - (height % 8))) / 8, proIndex = 0;

            for (int xBlock = 0; xBlock < xBlocks; xBlock++)
            {//for every horizontal block
                for (int yBlock = 0; yBlock < yBlocks; yBlock++)
                {//and for every vertical block
                    for (int x = 0; x < 8; x++)
                    {//we now go through our dedicated block starting at the first block (0,0) and moving on vertically then horizontally
                        for (int y = 0; y < 8; y++)
                        {
                            //if either X or Y is beyond the chroma channel's bounds, there will be nothing there, thus we pad with a zero
                            if (x + (8 * xBlock) < chromaChannel.GetLength(0) && y + (8 * yBlock) < chromaChannel.GetLength(1))
                                tmp[x, y] = chromaChannel[x + (8*xBlock), y + (8*yBlock)];//we must add the chromaChannel x/y with the particular block we're in times 8 to get the value we desire.
                            else
                                tmp[x, y] = 0;
                        }
                    }
                    //We have run through all 64 elements of this block
                    //now we process this block through DCT and move the tmp block into the larger processed block
                    tmp = DCT(tmp);
                    //now that we have DCT done, we may as well go on to quantize this 8x8 block. cause why not
                    //actually processing quantize in it's own function would take longer because we'd have to do the 8x8 for loop magic again
                    //and I'm not keen on that.
                    tmp = quantize(tmp, qTable);
                    //now we want to run zig-zag ordering to make things easier to encode. this will lead us into creating a 
                    //one dimensional array, which will be returned to be encoded.
                    zTmp = zcoding(tmp);
                    //now we add zTmp to the end of the processed array
                    //we get the index we put this all into 
                    Array.Copy(zTmp, 0, processed, proIndex++ * 64, 64);//here we pull in the proIndex
                }
            }
            //cool, we have now processed the entire YUV channel
            return processed;
        }

        //We must assume that f is an 8x8 matrix array. 
        public static double[,] DCT(double[,] f)
        {
            double[,] newF = new double[8,8];
            double Cu = 0, Cv = 0, temp = 0;

            for(int u = 0; u < 8; u++)
            {
                for(int v = 0; v < 8; v++)
                {
                    //temp = 0;
                    for(int x = 0; x < 8; x++)
                    {
                        for(int y = 0; y < 8; y++)
                        {
                            if (u == 0)
                                Cu = 1 / Math.Sqrt(2);  // Had it before as Math.Sqrt(2) / 2
                            else
                                Cu = 1;

                            if (v == 0)
                                Cv = 1 / Math.Sqrt(2);
                            else
                                Cv = 1;

                             temp += Math.Cos((2 * x + 1) * u * Math.PI / 16) * Math.Cos((2 * y + 1) * v * Math.PI / 16) * f[x, y];
                        }
                    }
                    newF[u,v] = Cu * Cv * temp / 4;
                    temp = 0;
                }
            }
            return newF;
        }

        public static double[,] IDCT(double[,] F)
        {
            double[,] newF = new double[8,8];
            double temp = 0;
            double Cu, Cv;

            for (int x = 0; x < 8; x++)
            {
                for (int y = 0; y < 8; y++)
                {
                    for (int u = 0; u < 8; u++)
                    {
                        for (int v = 0; v < 8; v++)
                        {
                            if (u == 0)
                                Cu = 1 / Math.Sqrt(2); 
                            else
                                Cu = 1;

                            if (v == 0)
                                Cv = 1 / Math.Sqrt(2);
                            else
                                Cv = 1;

                            temp += Cv * Cu * F[u,v] * Math.Cos(u * Math.PI * (2 * x + 1) / 16) * Math.Cos(v * Math.PI * (2 * y + 1) / 16);
                        }
                    }
                    newF[x,y] = (int)Math.Round(temp / 4);
                    temp = 0;
                }
            }
            return newF;
        }

        public static double[,] quantize(double[,] f, int[] q)
        {
            double[,] newF = new double[8, 8];

            for (int i = 0, k = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++, k++)
                {
                    newF[i, j] = Math.Round(f[i, j] / q[k]);
                }
            }
            if (newF[0, 0] < 1 && newF[0, 0] > 0)
                newF[0, 0] = 1;
            else if (newF[0, 0] < 0 && newF[0, 0] > -1)
                newF[0, 0] = -1;
            else if (newF[0, 0] == 0)
                newF[0, 0] = 1;

            return newF;
        }

        public static double[,] iquantize(double[,] f, int[] q)
        {
            double[,] newF = new double[8, 8];

            for (int i = 0, k = 0; i < 8; i++)
                for (int j = 0; j < 8; j++, k++)
                    newF[i, j] = f[i, j] * q[k];

            return newF;
        }

        //chroma must be size 64 (8x8)
        public static double[] zcoding(double[,] chroma)
        {
            double[] zig = new double[64];
            int index = 0;

            zig[index++] = chroma[0, 0];
            zig[index++] = chroma[1, 0];
            zig[index++] = chroma[0, 1];
            zig[index++] = chroma[0, 2];
            zig[index++] = chroma[1, 1];
            zig[index++] = chroma[2, 0];
            zig[index++] = chroma[3, 0];
            zig[index++] = chroma[2, 1];
            zig[index++] = chroma[1, 2];
            zig[index++] = chroma[0, 3];
            zig[index++] = chroma[0, 4];
            zig[index++] = chroma[1, 3];
            zig[index++] = chroma[2, 2];
            zig[index++] = chroma[3, 1];
            zig[index++] = chroma[4, 0];
            zig[index++] = chroma[5, 0];
            zig[index++] = chroma[4, 1];
            zig[index++] = chroma[3, 2];
            zig[index++] = chroma[2, 3];
            zig[index++] = chroma[1, 4];
            zig[index++] = chroma[0, 5];
            zig[index++] = chroma[0, 6];
            zig[index++] = chroma[1, 5];
            zig[index++] = chroma[2, 4];
            zig[index++] = chroma[3, 3];
            zig[index++] = chroma[4, 2];
            zig[index++] = chroma[5, 1];
            zig[index++] = chroma[6, 0];
            zig[index++] = chroma[7, 0];
            zig[index++] = chroma[6, 1];
            zig[index++] = chroma[5, 2];
            zig[index++] = chroma[4, 3];
            zig[index++] = chroma[3, 4];
            zig[index++] = chroma[2, 5];
            zig[index++] = chroma[1, 6];
            zig[index++] = chroma[0, 7];
            zig[index++] = chroma[1, 7];
            zig[index++] = chroma[2, 6];
            zig[index++] = chroma[3, 5];
            zig[index++] = chroma[4, 4];
            zig[index++] = chroma[5, 3];
            zig[index++] = chroma[6, 2];
            zig[index++] = chroma[7, 1];
            zig[index++] = chroma[7, 2];
            zig[index++] = chroma[6, 3];
            zig[index++] = chroma[5, 4];
            zig[index++] = chroma[4, 5];
            zig[index++] = chroma[3, 6];
            zig[index++] = chroma[2, 7];
            zig[index++] = chroma[3, 7];
            zig[index++] = chroma[4, 6];
            zig[index++] = chroma[5, 5];
            zig[index++] = chroma[6, 4];
            zig[index++] = chroma[7, 3];
            zig[index++] = chroma[7, 4];
            zig[index++] = chroma[6, 5];
            zig[index++] = chroma[5, 6];
            zig[index++] = chroma[4, 7];
            zig[index++] = chroma[5, 7];
            zig[index++] = chroma[6, 6];
            zig[index++] = chroma[7, 5];
            zig[index++] = chroma[7, 6];
            zig[index++] = chroma[6, 7];
            zig[index++] = chroma[7, 7];

            return zig;
        }

        public static byte[] runLengthEncoding(double[] stream)
        {
            //okay, so this is going to be fun. I have to encode a new array that I don't know the future length of
            //so I'm going to use an arraylist. 
            List<int> encode = new List<int>();
            //we need to convert the ints to SBytes, then the SBytes to bytes so it can be saved. de-encoding will be a bit more complex
            SByte[] coded; //didn't think I was leaving us at arraylists, did ya? this will be for the eventual return.
            byte[] convert;//the bytes we will be having
            int zeros = 0; //this will theoretically keep track of the length of our new byte array and how many zeros in the run.

           /* try
            {
                ostrm = new FileStream("./stream_pre_RLE.txt", FileMode.OpenOrCreate, FileAccess.Write);
                writer = new StreamWriter(ostrm);
                Console.SetOut(writer);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot open ./stream_pre_RLE.txt for writing");
                Console.WriteLine(e.Message);
            }*/

            //now what we want to do is go through our entire stream and count how many zeros we have, using zero as 
            //the delimiter to determine RLE. so if I read in a zero, the immediate following number is how many zeros there are.
            for (int iii = 0; iii < stream.Length; iii++)
            {
                //oh finally, just a one dimensional for loop for once. whooooo!
                //we're going to pump everything into coded as an int! woo!
                //Then afterwards we will copy the arraylist to the coded array
                //in theory, and as far as I have seen, nothing should be larger than 255, but if it is, I'll add a check just in case
                if (stream[iii] > 255)
                {
                    //Console.WriteLine("well shucks.");//useful, no?
                    stream[iii] = 255;
                }

                //if we don't hit a zero and we haven't hit a zero, we stop here and pump it in
                if (zeros == 0 && stream[iii] != 0)
                {
                    encode.Add((int)stream[iii]);//we hit our first rounding error fun here :D
                }
                else if(stream[iii] == 0)//if we hit a zero we start counting and we add nothing.
                {
                    zeros++;
                }
                else if(stream[iii] != 0 && zeros > 0)//so now we have hit a nonzero, but zeros have been counting this whole time, so we add a bunch of stuff
                {
                    //first we add the zero, that's the important part
                    encode.Add(0);
                    //next we add the amount of zeros
                    encode.Add(zeros);
                    //better clear out that count now
                    zeros = 0;
                    //now we add the number we actually hit
                    encode.Add((int)stream[iii]);
                    //cool
                }
                //  Console.WriteLine(stream[iii]); 
            }
           /* Console.SetOut(oldOut);
            writer.Close();
            ostrm.Close();

            try
            {
                ostrm = new FileStream("./encode_post_RLE.txt", FileMode.OpenOrCreate, FileAccess.Write);
                writer = new StreamWriter(ostrm);
                Console.SetOut(writer);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot open ./encode_post_RLE.txt for writing");
                Console.WriteLine(e.Message);
            }*/
            //now our arraylist is populated, time to make coded and convert large enough to fit it
            coded = new SByte[encode.Count];
            convert = new byte[encode.Count];
            //and now to move everything from encode into code and turn it into a byte. guess what! this means another for loop!

            for(int iii = 0; iii < encode.Count; iii++)
            {
                //because there's signed integers in our results, we first must check to make sure they don't exceed 127 or -128
                if(encode[iii] > 127)
                {
                    encode[iii] = 127;
                }
                else if (encode[iii] < -128)
                {
                    encode[iii] = -128;
                }
                //tmp = Convert.ToSByte(encode[iii]);//we now convert the value to an SByte
                //nevermind, that didn't work. we're just going to add 127 to the value, now that we've bound it
                //encode[iii] = encode[iii] + 128;//temporarily removing this because andrei says so
                
                coded[iii] = Convert.ToSByte(encode[iii]);//then the int to a SByte
                                                          //Console.WriteLine("WE ARE ENCODING " + coded[iii]);
                                                          //Console.WriteLine("WE HAVE CODED " + coded[iii]);
                                                          //TAKE THAT
                
                //Console.WriteLine(coded[iii]);
            }
            convert = (byte[])(Array)coded;//then SByte to Byte
                                           //cool
           /* Console.SetOut(oldOut);
            writer.Close();
            ostrm.Close();*/

            Console.WriteLine("ENCODING COMPLETE");
            return convert;
        }

        public static double[] undoEncoding(SByte[] stream, int size, double[] preRLE)
        {
            //well how about that, we now have to undo the encoding we did previously! what a shock
            //fortunately for me, I'm a genius
            //what we have to do first is make a new double array that will fit our original size. fortunaly my genius put this number into the file type, so woo for that
            double[] decipher = new double[size];
            int index = 0; //this is our decipher index, since it will need to rush ahead of the stream
            int[] tmp = new int[stream.Length];
            //We also need an SByte stream, because all these bytes are secretly signed!
            //they thought I wouldn't notice BUT I NOTICED!!
            //SByte[] convert = (SByte[])(Array)stream;//apparently this conversion is done completely different than any other conversion in C# ever. don't ask me why.

            //because we added 127 to all the values to make them bytes, we have to take that away. so we must now convert
            //all those bytes to ints. woooooo
            Array.Copy(stream, tmp, stream.Length);
/*
            try
            {
                ostrm = new FileStream("./stream_pre_URLE.txt", FileMode.OpenOrCreate, FileAccess.Write);
                writer = new StreamWriter(ostrm);
                Console.SetOut(writer);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot open ./stream_pre_URLE.txt for writing");
                Console.WriteLine(e.Message);
            }
            */
            //now what we have to do is run a for loop that will go through the stream, but there's a catch
            //we hit zeros, we need to repopulate all of them
            for (int iii = 0; iii < stream.Length; iii++)
            {
                //first, we have to subtract 127 from the value, since we added it to make everything over 0
               // tmp[iii] -= 128;//now we should have our original byte values
                //Console.WriteLine("WE ARE DEENCODING " + tmp[iii] + " THE ORIGINAL OF WHICH IS " + (stream[iii]));
                if (tmp[iii] != 0)//if we don't hit a zero, we just copy it over. don't worry, I have a plan for if we do.
                {
                    decipher[index++] = tmp[iii];
                }
                else if(tmp[iii] == 0)//I told you I had a plan! what we're going to do is skip ahead one, but first
                {
                    //decipher[index++] = tmp[iii++]; //we copy the original zero over, then we bump stream over one
                    //now we enter another for loop that will fill decipher with zeros 
                    iii++;
                    for(int jjj = 0; jjj < tmp[iii]; jjj++, index++)
                    {
                        //in essence, we check jjj against the next number in the stream and add that many zeros, then we bump up the index too!
                        decipher[index] = 0;

                    }
                    //and now we bump up iii
                    //iii++;
                }
                //and that should be it
            }
            /*  foreach (int iii in stream)
                  Console.WriteLine(iii);
              Console.SetOut(oldOut);
              writer.Close();
              ostrm.Close();

              try
              {
                  ostrm = new FileStream("./stream_post_URLE.txt", FileMode.OpenOrCreate, FileAccess.Write);
                  writer = new StreamWriter(ostrm);
                  Console.SetOut(writer);
              }
              catch (Exception e)
              {
                  Console.WriteLine("Cannot open ./stream_post_URLE.txt for writing");
                  Console.WriteLine(e.Message);
              }


               foreach (int iii in decipher)
                    Console.WriteLine(iii);

              Console.SetOut(oldOut);
              writer.Close();
              ostrm.Close();
             
            if(decipher.Length != preRLE.Length)
            {
                Console.WriteLine("Array Length Mismatch. RLE may have failed somewhere. Decipher Length = " + decipher.Length + "; preRLE length = " + preRLE.Length);
            }

            for(int lll = 0; lll < decipher.Length && lll < preRLE.Length; lll++)
            {
                if(decipher[lll] != preRLE[lll])
                {
                    Console.WriteLine("Run Length Encoding Mismatch at " + lll + ". decipher = " + decipher[lll] + "; preRLE = " + preRLE[lll]);
                }
            }
             */
            return decipher;
        }

        public static double[,] undoProcessing(int width, int height, double[] stream, int[]qTable)
        {
            //the width and height provided for this are assumed correct for the stream we're processing
            //what we have to do is take the stream and for each section of 64, apply it to an 8x8 matrix
            //we have to undo zig-zag encoding, undo quantization, and then undo DCT, this should result in our
            //regular luminance channel again, from which if it's Cr or Cb, can be "decompressed"
            //first thing we have to do is split the stream into 64 unit chunks

            double[] blockTmp = new double[64];
            double[,] tmp = new double[8, 8], chroma = new double[width, height], 
                      tmpChroma = new double[width + (8 - (width%8)), height + (8- (height%8))];
            //here is very simple. We calculate how many blocks are in the X and Y directions, and we have counters to keep track of both
            //we go in the Y direction first, as we always do, then we go in the X direction. When we hit as many Y blocks as can fit, we 
            //reset that to 0 and increment x
            int xBlocks = (width + (8 - (width % 8))) / 8, yBlocks = (height + (8 - (height % 8))) / 8, xBlock = 0, yBlock = 0,
                greaterX, greaterY;
            //here's where our original problem lies. if we have a width and height not divisible by 8 we want a temp block where it does
            //then, after we've undone all our data and moved it all back into it's appropriate slots, we want to dump all the extraneous
            //data (which should b zero or close-to zero) off the ends.

            for(int block = 0; block < stream.Length; block += 64)
            {
                //essentially we skip forward each 64 units every time we make this lovely block.
                //now we have to throw the 64 units into a tmp block of 64 and then process de-zigzagging, which will 
                //appropriately apply these units into an 8x8 grid
                Array.Copy(stream, block, blockTmp, 0, 64);//aaaaaaaaaaaand copied
                //undo the ZCoding
                tmp = undoZCoding(blockTmp);
                //from here we have to apply inverse quantization. good thing I got that working earlier!
                //we apply our now de-zigged tmp and our proper quantization table and undo that nonsense
                tmp = iquantize(tmp, qTable);
                //next up is undoing DCT. don't need any of that DCT garbage clogging up our beautiful art
                //again we apply tmp
                tmp = IDCT(tmp);
                //now that we've done all the conversions, we now have to place it into it's appropriate spot
                //now we position the block into the tmpChroma x*8, y*8
                //this is going to need more for loops
                //bet you were wondering about those greaterX and greaterY variables, eh? those are to keep track of the
                //actual starting positions of our block in the larger array
                //really it's a small thing to keep down processing time. Just a touch.
                greaterX = (xBlock * 8);
                greaterY = (yBlock * 8);

                for (int x = 0; x < 8; x++)
                {
                    for(int y = 0; y < 8; y++)
                    {
                        //the for loop is simple enough, x,y map to our blockTmp
                        //the complex part is mapping to the larger block in tmp chroma. To do that we take the block we're in, multiply that by 8
                        //and then add our intrepid x,y values to it, this starts us in the right position, then lets us traverse it
                        tmpChroma[greaterX + x, greaterY + y] = tmp[x, y];
                    }
                }

                //here is very simple. We calculate how many blocks are in the X and Y directions, and we have counters to keep track of both
                //we go in the Y direction first, as we always do, then we go in the X direction. When we hit as many Y blocks as can fit, we 
                //reset that to 0 and increment x
                //yup, that's copy-pasta'd from above. don't judge me
                if (yBlock < yBlocks - 1)
                {
                    yBlock++;
                }
                else
                {
                    yBlock = 0;
                    xBlock++;
                }
            }
            //okay, we've undone compression, zigzagging, quantization, and DCT, and put all of our blocks into a larger chunk. NOWS THE TIME TO CUT IT!
            //first we'll check to make sure our width and height isn't the same as our modified 8-multiple tmp. because if it is, who cares? we'll just return that
            if (width == (width + width % 8) && height == (height + height % 8))
                return tmpChroma;

            //but likely we've gone over, so instead we need to cut them down to size by copying over only the data we want
            //you know what this means? MORE FOR LOOPS!
            for(int x = 0; x < width; x++)
            {
                for(int y = 0; y < height; y++)
                {
                    //again, simple enough. traverse through both arrays, copying over until we hit the max of the desired size of array
                    chroma[x, y] = tmpChroma[x, y];
                }
            }

            //and with all that done, we now return chroma! Woooooo!
            return chroma;
        }

        public static double[,] undoZCoding(double[] zig)
        {
            double[,] chroma = new double[8,8];
            int index = 0;
            //undoing the Zcoding should be simply applying each unit in seqence from the 1D array to
            //it's original position in the chroma array, and then returning the chroma array. makes sense to me.
            //its 2am

            chroma[0, 0] = zig[index++];
            chroma[1, 0] = zig[index++];
            chroma[0, 1] = zig[index++];
            chroma[0, 2] = zig[index++];
            chroma[1, 1] = zig[index++];
            chroma[2, 0] = zig[index++];
            chroma[3, 0] = zig[index++];
            chroma[2, 1] = zig[index++];
            chroma[1, 2] = zig[index++];
            chroma[0, 3] = zig[index++];
            chroma[0, 4] = zig[index++];
            chroma[1, 3] = zig[index++];
            chroma[2, 2] = zig[index++];
            chroma[3, 1] = zig[index++];
            chroma[4, 0] = zig[index++];
            chroma[5, 0] = zig[index++];
            chroma[4, 1] = zig[index++];
            chroma[3, 2] = zig[index++];
            chroma[2, 3] = zig[index++];
            chroma[1, 4] = zig[index++];
            chroma[0, 5] = zig[index++];
            chroma[0, 6] = zig[index++];
            chroma[1, 5] = zig[index++];
            chroma[2, 4] = zig[index++];
            chroma[3, 3] = zig[index++];
            chroma[4, 2] = zig[index++];
            chroma[5, 1] = zig[index++];
            chroma[6, 0] = zig[index++];
            chroma[7, 0] = zig[index++];
            chroma[6, 1] = zig[index++];
            chroma[5, 2] = zig[index++];
            chroma[4, 3] = zig[index++];
            chroma[3, 4] = zig[index++];
            chroma[2, 5] = zig[index++];
            chroma[1, 6] = zig[index++];
            chroma[0, 7] = zig[index++];
            chroma[1, 7] = zig[index++];
            chroma[2, 6] = zig[index++];
            chroma[3, 5] = zig[index++];
            chroma[4, 4] = zig[index++];
            chroma[5, 3] = zig[index++];
            chroma[6, 2] = zig[index++];
            chroma[7, 1] = zig[index++];
            chroma[7, 2] = zig[index++];
            chroma[6, 3] = zig[index++];
            chroma[5, 4] = zig[index++];
            chroma[4, 5] = zig[index++];
            chroma[3, 6] = zig[index++];
            chroma[2, 7] = zig[index++];
            chroma[3, 7] = zig[index++];
            chroma[4, 6] = zig[index++];
            chroma[5, 5] = zig[index++];
            chroma[6, 4] = zig[index++];
            chroma[7, 3] = zig[index++];
            chroma[7, 4] = zig[index++];
            chroma[6, 5] = zig[index++];
            chroma[5, 6] = zig[index++];
            chroma[4, 7] = zig[index++];
            chroma[5, 7] = zig[index++];
            chroma[6, 6] = zig[index++];
            chroma[7, 5] = zig[index++];
            chroma[7, 6] = zig[index++];
            chroma[6, 7] = zig[index++];
            chroma[7, 7] = zig[index++];

            return chroma;
        }

        public static double[,] expandChrominance(double[,] stream)
        {
            //so what we want to do here is duplicate the chrominance data vertically and horizontally to fill in the colours of 
            //the image
            //to do this we make an array twice as wide and twice as tall
            double[,] expansion = new double[stream.GetLength(0) * 2, stream.GetLength(1) * 2];

            //next, we need to traverse this array twice as fast as we traverse the stream, in order to keep pace, so to speak
            //get ready for some MORE FOR LOOPS!
            for(int x = 0, xp = 0; x < stream.GetLength(0); x++, xp += 2)
            {
                for(int y = 0, yp = 0; y < stream.GetLength(1); y++, yp += 2)
                {
                    //you might be curious about xp and yp.
                    //xp, yp will be traversing our larger array, filling it in twice as fast as we read from our stream
                    expansion[xp, yp] = stream[x, y];//the one we're at
                    expansion[xp + 1, yp] = stream[x, y];//the one next to it
                    expansion[xp + 1, yp + 1] = stream[x, y];//the one diagonal to it
                    expansion[xp, yp + 1] = stream[x, y];//the one below it
                }
            }
            return expansion;
        }
    }
}
