﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace H261Compression
{
    public partial class Form1 : Form
    {
        OpenFileDialog ofd;
        SaveFileDialog sfd;
        Loading loadBar;

        Form2 f2;

        JPEGImage jpgImage, lImage, rImage;

        string imgFile;

        double[,] testData2 =
        {
             { 70, 70, 100, 70, 87, 87, 150, 187 },
             { 85, 100, 96, 79, 87, 154, 87, 113 },
             {100, 85, 116, 79, 70, 87, 86, 196 },
             {136, 69, 87, 200, 79, 71, 117, 96 },
             {161, 70, 87, 200, 103, 71, 96, 113},
             {161, 123, 147, 133, 113, 113, 85, 61} ,
             {146, 147, 175, 100, 103, 103, 163,187} ,
             {156, 146, 189, 70, 113, 161, 163, 197 }
        }, testResult = new double[8,8], iTestResult = new double[8,8];

        int[,] intTestResult = new int[8, 8];

        public Form1()
        {
            InitializeComponent();
            loadBar = new Loading();
        }

        private void openToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string[] nameParts;//this is for some parsing once we get the file name
            //this will open the desired file
            ofd = new OpenFileDialog();
            //we want to be able to open either jpegs, bmps, and our own special custome jaypegs!
            ofd.Filter = "Bitmap (*.bmp)|*.bmp|Jpeg (*.jpg;*.jpeg)|*.jpg;*.jpeg|Jaypeg (*.jaypeg)|*.jaypeg";
            ofd.Multiselect = false;//turn off multiselect, we only want one!
            if(ofd.ShowDialog() != DialogResult.OK)
            {
                //if it doesn't open, do this
                Console.WriteLine("nope nope nope nope");
                return;
            }
            else
            {
                loadBar.Visible = true;
                loadBar.Refresh();
                //if opening works
                imgFile = ofd.FileName;
                //now we have to check the ending of the filename in order to deduce what type it is:
                nameParts = imgFile.Split('.');

                if(nameParts[nameParts.Length - 1].ToLower() == "bmp" || nameParts[nameParts.Length - 1].ToLower() == "jpeg" 
                    || nameParts[nameParts.Length - 1].ToLower() == "jpg")
                    //if it's one of our favourite image types, we treat it as normal
                    jpgImage = new JPEGImage(imgFile, JCompression.DATATYPE.BMP);
                else if(nameParts[nameParts.Length - 1].ToLower() == "jaypeg")
                    //if it's our super special awesome image type, we treat it special
                    jpgImage = new JPEGImage(imgFile, JCompression.DATATYPE.JAY);

                //now here we pull the data from the bmp and the three streams in order to populate our dear image thingy
                if (jpgImage.getBitmap() != null)
                {
                    this.pictureBox1.Image = jpgImage.getBitmap();
                    this.pictureBox2.Image = jpgImage.getLuminanceStream();
                    this.pictureBox3.Image = jpgImage.getYUVStream();
                    this.pictureBox4.Image = jpgImage.getBlueStream();
                }
                loadBar.Visible = false;
            }
            //yay, DCT works. Woo, now to quantize and De-Quantize
           /* testResult = JCompression.DCT(testData2);
            iTestResult = JCompression.quantize(testResult, JCompression.chrominanceTable);
            iTestResult = JCompression.iquantize(iTestResult, JCompression.chrominanceTable);
            iTestResult = JCompression.IDCT(testResult);*/
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void h261ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //we have moved the compression steps into a function below!
            
            loadBar.Visible = true;
            loadBar.Refresh();
            jpgImage.compressImage();
            loadBar.Visible = false;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //and here's the lovely thing we click in order to save our image as a .Jaypeg image. I'm so clever
            sfd = new SaveFileDialog();
            sfd.Filter = "Jaypeg Files (.jaypeg)|*.jaypeg";//to force our saved image into the right type
            sfd.FilterIndex = 0; //probably unnecessary, but you never know.
            sfd.SupportMultiDottedExtensions = false;//no need for this
            sfd.ValidateNames = true; //not actually sure what this is for
            //now we check the sfd opens
            if (sfd.ShowDialog() != DialogResult.OK)
            {
                //if it doesn't, throw an error
                Console.WriteLine("Error opening save file dialogue. this really should never happen.");//best error
            }
            else
            {
                loadBar.Visible = true;
                loadBar.Refresh();
                //if it does, save the thing~!
                jpgImage.saveBitmap(sfd.FileName);
                loadBar.Visible = false;
            }

        }

        private void detectMotionVectorsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //WOO howdy! okay what we need to do here is load in two images, run compression on them, 
            //then run motion vector detections on them. Sounds simple right? Sure. Sure it does.
            //first we need to call the first image using the OFD and run full compression on it.
            //copy pasta from above!
            string[] nameParts;//this is for some parsing once we get the file name
            //this will open the desired file
            ofd = new OpenFileDialog();
            //we want to be able to open either jpegs, bmps, and our own special custome jaypegs!
            ofd.Filter = "Bitmap (*.bmp)|*.bmp|Jpeg (*.jpg;*.jpeg)|*.jpg;*.jpeg|Jaypeg (*.jaypeg)|*.jaypeg";
            ofd.Multiselect = false;//turn off multiselect, we only want one!
            if (ofd.ShowDialog() != DialogResult.OK)
            {
                //if it doesn't open, do this
                Console.WriteLine("nope nope nope nope");
                return;
            }
            else
            {
                //if opening works
                imgFile = ofd.FileName;
                //now we have to check the ending of the filename in order to deduce what type it is:
                nameParts = imgFile.Split('.');

                if (nameParts[nameParts.Length - 1].ToLower() == "bmp" || nameParts[nameParts.Length - 1].ToLower() == "jpeg"
                    || nameParts[nameParts.Length - 1].ToLower() == "jpg")
                    //if it's one of our favourite image types, we treat it as normal
                    lImage = new JPEGImage(imgFile, JCompression.DATATYPE.BMP);
                else if (nameParts[nameParts.Length - 1].ToLower() == "jaypeg")
                    //if it's our super special awesome image type, we treat it special
                    lImage = new JPEGImage(imgFile, JCompression.DATATYPE.JAY);
                //difference here is we don't apply the BMP to the current form! just you wait!
                //now we run full compression on it immediately
                lImage.compressImage();
                lImage.saveBitmap(nameParts[0] + ".jaypeg");
                lImage = new JPEGImage(nameParts[0] + ".jaypeg", JCompression.DATATYPE.JAY);
                lImage.decompress();
            }

            //now we repeat most of these steps for the rImage, stopping right before compression for reasons unknown!
            if (ofd.ShowDialog() != DialogResult.OK)
            {
                //if it doesn't open, do this
                Console.WriteLine("nope nope nope nope");
                return;
            }
            else
            {
                //if opening works
                imgFile = ofd.FileName;
                //now we have to check the ending of the filename in order to deduce what type it is:
                nameParts = imgFile.Split('.');

                if (nameParts[nameParts.Length - 1].ToLower() == "bmp" || nameParts[nameParts.Length - 1].ToLower() == "jpeg"
                    || nameParts[nameParts.Length - 1].ToLower() == "jpg")
                    //if it's one of our favourite image types, we treat it as normal
                    rImage = new JPEGImage(imgFile, JCompression.DATATYPE.BMP);
                else if (nameParts[nameParts.Length - 1].ToLower() == "jaypeg")
                    //if it's our super special awesome image type, we treat it special
                    rImage = new JPEGImage(imgFile, JCompression.DATATYPE.JAY);
                //difference here is we don't apply the BMP to the current form! just you wait!
                //now we run full compression on it immediately
                rImage.compressImage();//we run compression because technically until it's saved, it's not compressed!
                rImage.saveBitmap(nameParts[0] + ".jaypeg");
                rImage = new JPEGImage(nameParts[0] + ".jaypeg", JCompression.DATATYPE.JAY);
                rImage.decompress();
            }

            //now we detect motion vectors!
            MotionVector.calculateMotionVectors(lImage, rImage);
            f2 = new Form2();
            f2.rightImage.Image = rImage.getBitmap();
            f2.leftImage.Image = lImage.getBitmap();
            f2.Show();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        
    }
}
