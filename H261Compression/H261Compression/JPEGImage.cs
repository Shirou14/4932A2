﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace H261Compression
{
    public class JPEGImage
    {
        private Bitmap bmp;
        //private byte[] R, G, B, Y, Cr, Cb;
        private Color[] colors;
        public int imgHeight, imgWidth, encY, encCr, encCb, extY, extCrCb;
        private int[,] R2, G2, B2;
        //yes, I know there's an ungodly number of arrays here, but I want to make sure EVERYTHING WORKS. 
        //I may shrink them down at the end. we'll see. shrug.
        public double[,] Y, Cr, Cb, compCr, compCb, dctY, dctCr, dctCb;
        public double[] qY, qCr, qCb, tmpY, tmpCr, tmpCb;
        public byte[] bY, bCr, bCb;
        public SByte[] sbY, sbCr, sbCb;

        private BinaryReader br;

        public JPEGImage(string file, JCompression.DATATYPE dt)
        {
            //if we have a bitmap, we want to import all the pixels as is into the RGB stream
            if(dt == JCompression.DATATYPE.BMP || dt == JCompression.DATATYPE.JPG)
            {
                bmp = (Bitmap)Bitmap.FromFile(file);
                imgHeight = bmp.Height;
                imgWidth = bmp.Width;

                RGBtoYUV();
            }
            else if(dt == JCompression.DATATYPE.JAY)
            {
                //now we need to decode my super special awesome file that I have encoded in order to do the thing
                //woo
                //first we have to read the thing
                br = new BinaryReader(File.OpenRead(file));
                //read in the width
                imgWidth = br.ReadInt32();
                //read the height
                imgHeight = br.ReadInt32();
                //read Y's length before encoding
                //ext stands for extended
                extY = br.ReadInt32();
                //read CrCb length before encoding
                extCrCb = br.ReadInt32();
                //Y length after encoding
                //enc stands for encoded
                encY = br.ReadInt32();
                //Cr length after encoding
                encCr = br.ReadInt32();
                //Cb length after encoding
                encCb = br.ReadInt32();
                //first we initialize everything as a size
                bY = new byte[encY];
                sbY = new SByte[encY];
                bCr = new byte[encCr];
                sbCr = new SByte[encCr];
                bCb = new byte[encCb];
                sbCb = new SByte[encCb];

                for (int yIndex = 0; yIndex < encY; yIndex++)
                {
                    sbY[yIndex] = br.ReadSByte();
                    bY[yIndex] = (byte)sbY[yIndex];
                }
                for (int crIndex = 0; crIndex < encCr; crIndex++)
                {
                    sbCr[crIndex] = br.ReadSByte();
                    bCr[crIndex] = (byte)sbCr[crIndex];
                }
                for (int cbIndex = 0; cbIndex < encCb; cbIndex++)
                {
                    sbCb[cbIndex] = br.ReadSByte();
                    bCb[cbIndex] = (byte)sbCb[cbIndex];
                }
                //with those, we get to read bytes until we fill up the encoded lengths
                //but APPARENTLY you can only read ONE SByte at a time. goody, goody, gumdrops.
                decompress();
            }
        }

        public void compressImage()
        {
            //jpgImage.RGBtoYUV(); //We were going to run RGBtoYUV here, but turns out I need it earlier. woops
            //now what I have been told is each value in the YUV channels need to be subtracted by 128. Thanks andrei

            //first step is to take the Cr, and Cb values and squish them down, taking every other row and column.
            compCb = JCompression.compressChrominance(Cb, imgWidth, imgHeight);
            compCr = JCompression.compressChrominance(Cr, imgWidth, imgHeight);
            //boy, all that destroying data is hard work!
            //Now what we want to do is run DCT on all three YUV channels. 
            //This could be threaded at some point, since all this data doesn't interact with each other.
            //due to complications and simplifications, process also quantizes and runs zigzag-ordering
            //Y is luminance, so it gets luminance table
            qY = JCompression.process(Y.GetLength(0), Y.GetLength(1), Y, JCompression.luminanceTable);
            //Cr is colour, so chrominance table!
            qCr = JCompression.process(compCr.GetLength(0), compCr.GetLength(1), compCr, JCompression.chrominanceTable);
            //same as above
            qCb = JCompression.process(compCb.GetLength(0), compCb.GetLength(1), compCb, JCompression.chrominanceTable);

            //well with that out of the way, who's up for a little run length encoding!
            bY = JCompression.runLengthEncoding(qY);
            JCompression.preY = qY;
            bCr = JCompression.runLengthEncoding(qCr);
            JCompression.preCr = qCr;
            //Array.Copy(qCr, tmpCr, qCr.Length);
            bCb = JCompression.runLengthEncoding(qCb);
            JCompression.preCb = qCb;
            //Array.Copy(qCb, tmpCb, qCb.Length);
            //oh don't we feel good and compressed now?
        }

        public void decompress()
        {

            //now that we have read those, the quantized arrays can be filled with the de-encoded arrays
            qY = JCompression.undoEncoding(sbY, extY, JCompression.preY);
            qCr = JCompression.undoEncoding(sbCr, extCrCb, JCompression.preCr);
            qCb = JCompression.undoEncoding(sbCb, extCrCb, JCompression.preCb);
            //now that we've done that, the fun part begins! De-processing!
            //since Y isn't compressed like CrCb, it can go right into the appropriate array
            Y = JCompression.undoProcessing(imgWidth, imgHeight, qY, JCompression.luminanceTable);
            //CrCb ain't so lucky
            compCr = JCompression.undoProcessing((int)Math.Ceiling((double)(imgWidth / 2)), (int)Math.Ceiling((double)(imgHeight / 2)),
                                                 qCr, JCompression.chrominanceTable);
            compCb = JCompression.undoProcessing((int)Math.Ceiling((double)(imgWidth / 2)), (int)Math.Ceiling((double)(imgHeight / 2)),
                                                 qCb, JCompression.chrominanceTable);
            //WHEW! Almost done! now we need to de-compress compCb and compCr basically by duplicating the data already there
            Cb = JCompression.expandChrominance(compCb);
            Cr = JCompression.expandChrominance(compCr);

            //so, in theory we have our decompressed and expanded YUV values. Now we need to convert these to our RGB values
            //once we do that we can display our lovely de-compressed image!
            YUVtoRGB();

            //now that everything is spread out, we need to re-istabish our bmp the size of our original image as spoken in days of old
            bmp = new Bitmap(imgWidth, imgHeight);

            //now all of our data is in R2, G2, an B2. we would like to apply these to our new bitmap!

            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < bmp.Width; x++)
                {
                    bmp.SetPixel(x, y, Color.FromArgb(R2[x, y], G2[x, y], B2[x, y]));
                }
            }
        }

        public void RGBtoYUV()
        {
            Y = new double[imgWidth, imgHeight];
            Cr = new double[imgWidth, imgHeight];
            Cb = new double[imgWidth, imgHeight];

            Rectangle rect = new Rectangle(0, 0, imgWidth, imgHeight);

            unsafe
            {
                for (int y = 0; y < imgHeight; y++)
                {
                    for (int x = 0; x < imgWidth; x++)
                    {
                        int xPor3 = x * 3;
                       // int index = x + (imgWidth * y);
                        int blue = bmp.GetPixel(x, y).B;//currentLine[xPor3++];
                        int green = bmp.GetPixel(x, y).G; //currentLine[xPor3++];
                        int red = bmp.GetPixel(x, y).R; //currentLine[xPor3];

                        //Console.WriteLine(x + ", " + y + "; R: " + red + ", G: " + green + ", B: " + blue);

                       /* R[index] = (byte)red;
                        G[index] = (byte)green;
                        B[index] = (byte)blue;*/

                        //Console.WriteLine(x + ", " + y + "; R: " + red + ", G: " + green + ", B: " + blue);
                        //Console.WriteLine("***************************");


                        Y[x, y] = 0.299 * red + 0.587 * green + 0.114 * blue;//((0.299 * red) + (0.587 * green) + (0.114 * blue));
                        Cb[x, y] = (128 - (0.168736 * red) - (0.331264 * green) + (0.5 * blue));
                        Cr[x, y] = (128 + (0.5 * red) - (0.418688 * green) - (0.081312 * blue));

                        Y[x, y] -= 128;
                        Cb[x, y] -= 128;
                        Cr[x, y] -= 128;
                        //Console.WriteLine(x + ", " + y + "; Y: " + Y2[index] + ", Cb: " + Cb2[index] + ", Cr: " + Cr2[index]);
                    }
                }
            }
        }

        private void YUVtoRGB()
        {
            R2 = new int[imgWidth, imgHeight];
            G2 = new int[imgWidth, imgHeight];
            B2 = new int[imgWidth, imgHeight];

            double tmpY = Y[0, 0], tmpCr = Cr[0, 0], tmpCb = Cb[0, 0];

            for(int y = 0; y < imgHeight; y++)
            {
                for (int x = 0; x < imgWidth; x++)
                {
                    //we're going to store the latest chroma value into a variable
                    if(x < Y.GetLength(0) && y < Y.GetLength(1))//we check to make sure we're not exceeding the bounds of the array
                    {
                        tmpY = Y[x, y];
                    }

                    //this check is largely for the Cr and Cb components, since due to compression they might not be the right size for the image. 
                    //thanks to compression their values are duplicated anyway, so duplicating them another time, if it's missing, isn't an issue.
                    if (x < Cr.GetLength(0) && y < Cr.GetLength(1))//we check to make sure we're not exceeding the bounds of the array
                    {
                        tmpCr = Cr[x, y];
                    }

                    if (x < Cb.GetLength(0) && y < Cb.GetLength(1))//we check to make sure we're not exceeding the bounds of the array
                    {
                        tmpCb = Cb[x, y];
                    }
                    //
                    //Console.WriteLine(c + ":: Y: " + Y2[c] + ", Cb: " + Cb2[c] + ", Cr: " + Cr2[c]);

                    R2[x,y] = JCompression.getRValue(tmpY, tmpCb, tmpCr);
                    if (R2[x,y] > 255)
                    {
                  //      Console.WriteLine("R2 at " + x + ", " + y + " is " + R2[x,y]);
                        R2[x,y] = 255;
                    }
                    else if (R2[x,y] < 0)
                    {
                    //    Console.WriteLine("R2 at " + x + ", " + y + " is " + R2[x,y]);
                        R2[x,y] = 0;
                    }
                    G2[x,y] = JCompression.getGValue(tmpY, tmpCb, tmpCr);
                    if (G2[x,y] > 255)
                    {
                   //     Console.WriteLine("G2 at " + x,y + " is " + G2[x,y]);
                        G2[x,y] = 255;
                    }
                    else if (G2[x,y] < 0)
                    {
                    //    Console.WriteLine("G2 at " + x + ", " + y + " is " + G2[x,y]);
                        G2[x,y] = 0;
                    }
                    B2[x,y] = JCompression.getBValue(tmpY, tmpCb, tmpCr);
                    if (B2[x,y] > 255)
                    {
                   //     Console.WriteLine("B2 at " + x + ", " + y + " is " + B2[x,y]);
                        B2[x,y] = 255;
                    }
                    else if (B2[x,y] < 0)
                    {
                    //    Console.WriteLine("B2 at " + x + ", " + y + " is " + B2[x,y]);
                        B2[x,y] = 0;
                    }
                }
            }
        }

        public Bitmap getBitmap()
        {
            return bmp;
        }

        public Bitmap getLuminanceStream()
        {
            //Stream rStream = new MemoryStream(R);
            Bitmap rBmp = new Bitmap(imgWidth, imgHeight);
            double tmpY = 0;
            //int x = -1, y = 0;

            for (int y = 0; y < imgHeight; y++)
            {
                for (int x = 0; x < imgWidth; x++)
                {
                    tmpY = Y[x, y] + 127;
                    if (tmpY > 255)
                    {
                       // Console.WriteLine("Y2 " + x + ", " + y + " = " + Y[x,y]);
                        tmpY = 255;
                    }
                    else if(tmpY < 0)
                    {
                        tmpY = 0;
                    }
                    rBmp.SetPixel(x, y, Color.FromArgb((int)tmpY, (int)tmpY, (int)tmpY));
                }
            }

            return rBmp;
        }

        public Bitmap getYUVStream()
        {
            //Stream rStream = new MemoryStream(R);
            Bitmap gBmp = new Bitmap(imgWidth, imgHeight);
            double tmpY = Y[0, 0], tmpCr = Cr[0, 0], tmpCb = Cb[0, 0];
            // int x = -1, y = 0;

            for (int y = 0; y < imgHeight; y++)
            {
                for (int x = 0; x < imgWidth; x++)
                {
                    /* tmpY = Y[x, y] + 128;
                     tmpCr = Cr[x, y] + 128;
                     tmpCb = Cb[x, y] + 128;
                     */

                    //we're going to store the latest chroma value into a variable
                    if (x < Y.GetLength(0) && y < Y.GetLength(1))//we check to make sure we're not exceeding the bounds of the array
                    {
                        tmpY = Y[x, y];
                    }

                    //this check is largely for the Cr and Cb components, since due to compression they might not be the right size for the image. 
                    //thanks to compression their values are duplicated anyway, so duplicating them another time, if it's missing, isn't an issue.
                    if (x < Cr.GetLength(0) && y < Cr.GetLength(1))//we check to make sure we're not exceeding the bounds of the array
                    {
                        tmpCr = Cr[x, y];
                    }

                    if (x < Cb.GetLength(0) && y < Cb.GetLength(1))//we check to make sure we're not exceeding the bounds of the array
                    {
                        tmpCb = Cb[x, y];
                    }

                    if (tmpY > 255)
                    {
                      //  Console.WriteLine("Y2 " + x + ", " + y + " = " + Y[x,y]);
                        tmpY = 255;

                    }
                    else if(tmpY < 0)
                    {
                        tmpY = 0;
                    }
                    if (tmpCr > 255)
                    {
                     //   Console.WriteLine("Cr2 " + x + ", " + y + " = " + Cr[x,y]);
                        tmpCr = 255;
                    }
                    else if(tmpCr < 0)
                    {
                        tmpCr = 0;
                    }
                    if (tmpCb > 255)
                    {
                     //   Console.WriteLine("Cb2 " + x + ", " + y + " = " + Cb[x,y]);
                        tmpCb = 255;
                    }
                    else if(tmpCb < 0)
                    {
                        tmpCb = 0;
                    }
                    gBmp.SetPixel(x, y, Color.FromArgb((int)tmpY, (int)tmpCb, (int)tmpCr));
                }
            }

            return gBmp;
        }

        public Bitmap getBlueStream()
        {
            //Stream rStream = new MemoryStream(R);
            YUVtoRGB();
            Bitmap bBmp = new Bitmap(imgWidth, imgHeight);
            
            for (int y = 0; y < R2.GetLength(1); y++)
            {
                for (int x = 0; x < R2.GetLength(0); x++)
                {
                    Color col = Color.FromArgb(R2[x,y], G2[x,y], B2[x,y]);

                    bBmp.SetPixel(x, y, col);
                }
            }

            return bBmp;
        }

        public void saveBitmap(string file)
        {
            //first we want to make sure the file is compressed.
            compressImage();
            //so here we are, finally saving my magical picture. It's way too late to still be working on this.
            BinaryWriter bw = new BinaryWriter(File.OpenWrite(file));//lets get ourselves a bard to write down this majesty
            //first thing we need to to is organize all the data we have
            //first thing saved, image width
            bw.Write((int)imgWidth);
            //image height
            bw.Write((int)imgHeight);
            //Y before encoding
            bw.Write((int)qY.Length);
            //Cr and Cb before encoding
            bw.Write((int)qCr.Length);
            //Y length after encoding
            bw.Write((int)bY.Length);//this is to know how long to keep reading the Y
            //Cr and Cb length after encoding (both should be the same)
            bw.Write((int)bCr.Length);//and this is to know how long to keep readin Cr and Cb
            //actually, as it turns out, for some reason, somehow, cr and cb are different legths now. Woo.
            //my bad, both should have different, although similar, sizes after runlength encoding. I'm stupid.
            bw.Write((int)bCb.Length);
            //now that the header is done we want to write the actual data! woooooo!
            bw.Write(bY, 0, bY.Length);
            bw.Write(bCr, 0, bCr.Length);
            bw.Write(bCb, 0, bCb.Length);
            //now we're done
            bw.Close();
        }
    }
}
