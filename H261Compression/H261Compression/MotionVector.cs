﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace H261Compression
{

    public struct Blocks
    {
        public double[,] block;
    }

    public struct UV
    {
        public int u;
        public int v;
        public int x;
        public int y;
    }

    public static class MotionVector
    {
        /*
         * 
         * OKAY CHUCKLE TRUCKS. let's get this motion vector magic going!
         * What we need to do is follow these steps!
         * Load in the JPEG (IFrame) and the Pframe (the second jpeg, essentially)
         * compress the IFrame, encode it, then decode it (awesome)
         * next we need to take the Y Cr or Cb of the IFrame and the PFrame and break them up into 8x8 blocks
         * now, for every center of the 8x8 block in the PFrame, we need to check the IFrame around it 15 pixels in each direction, creating a 32x32 super block
         * for each check, we take an 8x8 block of the first 8x8 pixels in the IFrame and sum the absolute difference of every pixel together
         * we compare these differences against a "smallest" number, because we're checking to see if there's a section that is the same as our selected section. 
         * the smallest number is the objective of our motion vector. From there we can draw a line from the center of our PFrame to the center of the unit vector.
         * FROM THERE, for each 8x8 block we save the block, run DCT, encoding, and all that general stuff, then we also save the motion vectors
         * I honestly don't know if I'll have time to bother with file saving. 
         * 
         */ 

        public static UV[] YMV, CrMV, CbMV, ylocations, crlocations; //this is the array of motion vectors
        public static Blocks[,] YIBlocks, CrIBlocks, YPBlocks, CrPBlocks;
        
        //okay, now to start calculating the two frames. 
        //we'll start with a function that takes in both an IFrame and a PFrame
        public static void calculateMotionVectors(JPEGImage IFrame, JPEGImage PFrame)
        {
            //so, we are assuming the IFrame and PFrame are already compressed and decompressed. don't you worry, I coded it this way.
            //first thing we need to do is break them up into 8x8 blocks. I stole the struct idea from Andrei. Thanks Andrei
            //first the IFrame
            YIBlocks = populateBlocks(IFrame.Y);
            CrIBlocks= populateBlocks(IFrame.compCr);//remember to use the subsampled Cr and not the full thing, kids!
            //now we do this for the PFrame
            YPBlocks = populateBlocks(PFrame.Y);
            CrPBlocks = populateBlocks(PFrame.compCr);

            //now that we have broken the images down into their component blocks, we want to go through each block in the PFrame, and check it 
            //against a 32x32 block in the IFrame until we find the appropriate motion vectors
            YMV = findMotionVectors(IFrame.Y, YIBlocks, YPBlocks);
            CrMV = findMotionVectors(IFrame.compCr, CrIBlocks, CrPBlocks);

            //now that we have found these magical motion vectors we need to... uhhh... hmmm
            //from here we want to draw the line on the image
            drawOnImage(PFrame);


        }

        public static Blocks[,] populateBlocks(double[,] chroma)//I also stole this function name, but there wasn't anything better i could think of this late at night. sue me
        {
            //Blocks are a structure of double[,]'s, so we can make a 2d array of blocks, which becomes a 2d array of doubles[,]
            //so if we break up the chroma height and width into values of 8, we can apply each value into a block, sorted by position
            //rounded up so we know we get all values
            Blocks[,] blk = new Blocks[(int)Math.Ceiling((double)chroma.GetLength(0)/8), (int)Math.Ceiling((double)chroma.GetLength(1) / 8)];
            double tmp;//this is to hold our value so we can easily buffer

            //now that we've initialized the new block, we need to populate them.
            //what we need to do is traverse the blk array both vertically and horizontally, and then fill it with the appropriate data from 
            //the chroma channel
            //this means more for loops. wooooooooo my buddy!
            for(int blockX = 0; blockX < blk.GetLength(0); blockX++)
            {
                //traversing the horizontal wasteland
                for(int blockY = 0; blockY < blk.GetLength(1); blockY++)
                {
                    //now for each block we actually have to initiate the double array inside
                    blk[blockX, blockY].block = new double[8, 8];

                    //traversing the vertical wasteland
                    //now what we have to do is traverse the actual chroma channel, if we go beyond the bonds of the channel, we want to pad with 0
                    for(int xPix = 0; xPix < 8; xPix++)
                    {
                        for(int yPix = 0; yPix < 8; yPix++)
                        {
                            if ((xPix + 8 * blockX) >= chroma.GetLength(0) || (yPix + 8 * blockY) >= chroma.GetLength(1))
                            {
                                //basically, if we're reaching for a coordinate that's not in the chroma array, we pad with 0
                                tmp = 0;
                            }
                            else
                                tmp = chroma[(xPix + 8 * blockX), (yPix + 8 * blockY)];
                            //otherwise we want to grab the double value in the array
                            blk[blockX, blockY].block[xPix, yPix] = tmp;
                        }
                    }

                }
            }
            return blk;

        }
       
        public static UV[] findMotionVectors(double[,] IFrameChannel, Blocks[,] IFrame, Blocks[,] PFrame)
        {
            UV[] mv = new UV[PFrame.GetLength(0) * PFrame.GetLength(1)];//our new motion vector array, which will be initialized to the size of the PFrame blocks. 
            //Each block in the PFrame get's it's own fancy motion vector
            //this will be the motion vector that we'll put into the array, we initialize everything to be zero just in case
            UV vector, source, dest;//source and dest are the PFrame and destination in the IFrame respectively.
            vector.u = 0;
            vector.v = 0;
            vector.x = 0;
            vector.y = 0;
            source.u = 0;
            source.v = 0;
            dest.u = 0;
            dest.v = 0;
            //so many INITLIZATIONS! WOWEE!
            double smallest = 0;
            double madSum = 0;
            double[,] block32; //this is our large block we're searching for the motion vector from
            double[,] sample = new double[8, 8];//this is the sample block from the block32 that we'll be comparing against, taking at every 8x8 permutation

            int p = 15;//this is the distance from the center of our PBlock to search in the image
            
            //now we must traverse the treacherous planes of the PFrame blocks
            for(int xBlock = 0; xBlock < PFrame.GetLength(0); xBlock++)
            {
                for(int yBlock = 0; yBlock < PFrame.GetLength(1); yBlock++)
                {
                    //so, for each block in the pframe, we must calculate the motion vector. but FIRST! We will calculate the motion vector
                    //pretending no motion has been made. AKA we directly check the 8x8 PFrame block with the corresponding 8x8 IFrame block
                    //the purpose of this is to avoid the issue where if there is no movement in the image, it won't point to the top right hand corner
                    //instead having the motion vector point at itself, the theory being if there's no movement, the vector will be zero.
                    //so what we want to do is subtract every value (pixel) in the PFrame block from it's corresponding pixel in the IFrame block
                    //and add together their absolute values into a sum.
                    smallest = MAD(IFrame[xBlock, yBlock].block, PFrame[xBlock, yBlock].block);
                    //may as well replace the location vector here
                    source.u = (xBlock * 8) + 4;//remember, we want the center of the block!
                    source.v = (yBlock * 8) + 4;
                    //now we'll check if smallest is near zero, if it is, we won't bother doing the rest because that's a waste of cycles.
                    if (smallest > 0.000001f)//honestly if it's smaller than this, we move on to the next block, no significant difference is available
                    {
                        //now what we want to do is for every 8x8 block in the 32x32 macro block, starting at the center of the PFrame block,
                        //compare the madSum against the smallest, if it's smaller than smallest, we keep track of the center of the IFrame block,
                        //put that into the destination vector, then replace smallest with our mad sum.

                        //first what we must do is create a 32x32 block around the center of our PFrame block from the IFrame
                        block32 = createBlock(IFrameChannel, (4 + (xBlock * 8)), (4 + (yBlock * 8)), p);

                        //now that we have our lovely block32, we need to check every permutation of it for our motion vector.
                        for(int xBeta = 0; xBeta < block32.GetLength(0) - 8; xBeta++)
                        {
                            //we stop incrementing xBeta at 8 less because we're checking an 8x8 block worth of pixels. We have no interest in looking past it.
                            for(int yBeta = 0; yBeta < block32.GetLength(1) - 8; yBeta++)
                            {
                                //now we know where we're starting from, xBeta,yBeta, we don't need to make another block
                                //instead what we're going to do is go forward in each direction by 8 and apply mad 
                                for(int xOmega = 0; xOmega < 8; xOmega++)
                                {
                                    for(int yOmega = 0; yOmega < 8; yOmega++)
                                    {
                                        //basically we're grabbing the pixel values from the larger block by way of it's position, plus our position inside it
                                        sample[xOmega, yOmega] = block32[xOmega + xBeta, yOmega + yBeta];
                                    }
                                }
                                //now that we've grabbed the 8x8 sample, we want to calculate MAD
                                madSum = MAD(sample, PFrame[xBlock, yBlock].block);
                                if(madSum < smallest)
                                {
                                    //if the madSum is smaller than smallest, we have a closer image match. if that's the case, 
                                    //we replace smallest, the destination vector, and the motion vector
                                    smallest = madSum;
                                    //oh getting the center of the destination block will be something!
                                    //what we want to do is add 4 to PFrame location - p plus however many X,Y  we've moved minus how far
                                    //past the edge we've gone in the negatives
                                    dest.u = 4 + Math.Max(xBlock * 8 - p, 0) + block32.GetLength(0) + (block32.GetLength(0) - xBeta);//ehh, we'll try this and see
                                    dest.v = 4 + Math.Max(yBlock * 8 - p, 0) + block32.GetLength(1) + (block32.GetLength(1) - yBeta);

                                    vector.u = source.u - dest.u;
                                    vector.v = source.v - dest.v;
                                    vector.x = source.u;
                                    vector.y = source.v;
                                }
                            }
                        }
                        //so we have a one dimensional motion vector array, so we're putting the motion vector into the index determined
                        //by the column times the height plus the row. so in theory if you picked up each block starting at the top, and going down,
                        //then when you reach the bottom starting at the top again, you'd have the same order as this motion vector 
                        mv[(xBlock * PFrame.GetLength(1)) + yBlock] = vector;
                    }
                    else
                    {
                        vector.u = 0;
                        vector.v = 0;
                        //so we have a one dimensional motion vector array, so we're putting the motion vector into the index determined
                        //by the column times the height plus the row. so in theory if you picked up each block starting at the top, and going down,
                        //then when you reach the bottom starting at the top again, you'd have the same order as this motion vector 
                        mv[(xBlock * PFrame.GetLength(1)) + yBlock] = vector;
                    }
                }
            }

            return mv;
        } 

        public static double MAD(double[,] IFrame, double[,] PFrame)//they'll get you next time, gadget.
        {
            //MAD stands for mean absolute difference. for the sake of simplicity, ignore the mean part. this means averaging it out, but since
            //all the blocks are the same size, it's not really necessary. I think. You should maybe ask dennis about it. Not me.
            //point is, we're going to subtract every pixel in one array from the other, absolute that, and add everything into a lovely sum, which
            //will be sent out
            double sum = 0;//here's our tidy sum

            //traversing the frames
            for(int x = 0; x < 8; x++)//yes, we assume the block given is 8x8, this whole thing would fall apart if it wasn't anyway
            {
                for(int y = 0; y < 8; y++)
                {
                    sum += Math.Abs(IFrame[x, y] - PFrame[x, y]);//if we wanted the mean, we could divide this whole thing by 64 but why bother?
                }
            }

            return sum;
        }

        public static double[,] createBlock(double[,] channel, int x, int y, int p)
        {
            //okay this had to go through a re-write, so lets see if I can get this working.
            //instead of grabbing an ensemble of blocks and tryingto deal with that, I'm using the raw, unblocked channel from the
            //IFrame and starting from the x,y co-ordinate which is the center of the PBlock, and moving left and up by a factor of P
            //I will be assuming the double[,] block is the size of p^4, going p in both directions from the center in both the X and Y directions
            //then after the whole thing is done, I will make a new double the size of the actual values in the old double, and return that, assuming it's
            //not p^4
            
            int xTrack = 0, yTrack = 0, yPermTrack = 0;//these keep track of how many values we're actually putting into neighbours
            int pSqr = p * p;//going to do this math a lot, may as well save some cycles
                double[,] neighbours = new double[pSqr, pSqr], trimmed;//neighbours is our base p^4 array, trimmed will be the proper sized one.
            //for loops ahoy!
            for (int xPrime = x - p, xCount = 0; xCount < pSqr; xPrime++, xCount++)
            {
                for(int yPrime = y - p, yCount = 0; yCount < pSqr; yPrime++, yCount++)
                {
                    if((xPrime > 0 && xPrime < channel.GetLength(0)) && (yPrime > 0 && yPrime < channel.GetLength(1)))//now we check to make sure it's within bounds
                    {
                        //if we're within bounds, we grab this element from the channel and put it into the neighbours channel
                        neighbours[xTrack, yTrack++] = channel[xPrime, yPrime];//we increment y here because if we are in bounds, we've just put into the Y position
                    }
                }
                if((xPrime > 0 && xPrime < channel.GetLength(0)))//now at the end here, we check to make sure the X is in bounds still, if it wasn't we don't want to increment it
                {
                    //if it was though, we want to set Y to zero, and X to +=1
                    xTrack += 1;
                    //now of course we run into the issue where we are no longer tracking the y properly, simply we'll put that into
                    //another Y tracking variable and check to make sure this new variable is larger than yTrack, if it is, it stays the same, if not
                    //then it will be incremented to yTrack before it's turned to 0
                    if (yTrack > yPermTrack)
                        yPermTrack = yTrack;
                    yTrack = 0; 
                }
            }
            //cool we've now just moved all the elements into the double array, now we want to create a new array of the appropriate size and 
            //return that. It's going to involve more for loops, but to avoid this we see if the array is full as expected
            if (xTrack == pSqr && yPermTrack == pSqr)
                return neighbours;

            //if this function is still running, well, it's not as full as we would like, so we have to do what we said
            trimmed = new double[xTrack, yPermTrack];
            //now for another set of for loops!
            for(int xPrime = 0; xPrime < xTrack; xPrime++)
            {
                for(int yPrime = 0; yPrime < yPermTrack; yPrime++)
                {
                    //at least by being a direct copy, this is pretty simple
                    trimmed[xPrime, yPrime] = neighbours[xPrime, yPrime];
                }
            }

            return trimmed;
        }

        public static void drawOnImage(JPEGImage jpg)
        {
            int count = 0;

            using (Graphics g = Graphics.FromImage(jpg.getBitmap()))
            {
                foreach (UV pnt in YMV)
                {
                    if (pnt.u != 0 && pnt.v != 0)
                    {
//                        x = (count / YPBlocks.GetLength(0)) * 8 + 4;
  //                      y = (count % YPBlocks.GetLength(1)) * 8 + 4;

                        g.DrawLine(new Pen(Color.Green), new Point(pnt.x, pnt.y),
                            new Point(pnt.u + pnt.x, pnt.v + pnt.y));
                    }
                    count++;
                }
            }
        }
    }
}
